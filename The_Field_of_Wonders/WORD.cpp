#include "WORD.h"

WORD::WORD(int chastota, string slovo)
{
	this->chastota = chastota;
	this->slovo = slovo;
}

ostream& operator<<(ostream& out, const WORD& word)
{
	out << word.slovo << "	" << word.chastota << endl;
	return out;
}