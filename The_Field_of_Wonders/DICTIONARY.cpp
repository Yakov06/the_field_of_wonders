#include "DICTIONARY.h"
#include <string>
#include <fstream>

vector<WORD*> DICTIONARY::filter(const vector<WORD*>& fd, int location, char letter)
{
	vector<WORD*> otfilt;
	for (int i = 0; i < fd.size(); i++) {
		if (fd[i]->slovo[location - 1] == letter) {
			otfilt.push_back(fd[i]);
		}
	}
	return otfilt;
}

DICTIONARY::DICTIONARY()
{
	ifstream fin;
	fin.open("1grams-3.txt");
	if (fin.is_open()) cout << "OK" << endl;
	else cout << "NOT" << endl;
	int a;
	while (!fin.eof()) {

		string slovo = "";
		string chastota = "";
		int count = 0;
		char stroka[256];

		fin.getline(stroka, 256);

		if (fin.eof()) break;

		for (int i = 0; i < strlen(stroka); i++) {
			if (stroka[i] != '\t' && count < 1) {
				chastota += stroka[i];
			}
			if (stroka[i] == '\t') count++;
			if (count >= 1) slovo += stroka[i];
		}
		int chislo = stoi(chastota);
		push_back(new WORD(chislo, slovo));
	}
}

void DICTIONARY::push_back(WORD* word)
{
	array.push_back(word);
}

void DICTIONARY::guess()
{
	int count;
	cout << "Enter kol_let: " << endl;
	cin >> count;
	vector<WORD*> temp;

	for (int i = 0; i < array.size(); i++) {
		if (array[i]->slovo.length() == count) temp.push_back(array[i]);
	}
	while (true) {
		cout << "Tell me the hint: " << endl;
		char letter;
		int loc;
		cin >> letter >> loc;
		temp = filter(temp, loc, letter);
		cout << "You guess: " << temp[0] << "?" << endl;
		string answer;
		cin >> answer;
		if (answer != "n" && answer != "N") break;
	}
}


ostream& operator<<(ostream& out, const DICTIONARY& dic)
{
	for (int i = 0; i < dic.array.size() - 1; i++) {
		out << "WORD (" << *dic.array[i] << ")" << endl;
	}
	out << "WORD (" << *dic.array[dic.array.size() - 1] << ")" << endl;
	return out;
}