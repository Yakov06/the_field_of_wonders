#pragma once
#include <iostream>
#include "WORD.h"
#include <vector>

class DICTIONARY
{
private:
	vector<WORD*> array;
	int count;
	vector<WORD*> filter(const vector<WORD*>& fd, int location, char letter);
	
public:
	DICTIONARY(); // ��������� ������ � �����
	void push_back(WORD* word); // ��������� ����� � array
	void guess(); // �������� ��������� ����������
	friend ostream& operator<< (ostream& out, const DICTIONARY& dic);
};